# 图卷积神经网络模型(GCN)

## 介绍
图卷积神经网络模型(Graph Convolutional Networks)  
参考论文：[Semi-Supervised Classification With Graph Convolutional Networks. ICLR 2017](https://arxiv.org/pdf/1609.02907.pdf)

## 模型说明
- data: 包含citeseer, cora_v2, pubmed三个数据集
- gcn: 图卷积神经网络模型(GCN), 相关参数可以使用Config类配置
- pic: 运行效果图

## 使用准备
- dgl >= 0.6.1
1. Anaconda （版本最好不要太旧）
2. torch==1.6.0; torch_vision==0.7.0 及相关的包 （或者更高版本，可自行调试）
3. 其他相关的包，可自行调试判断

## 使用说明
- 本代码仅供学习和学术研究下载

## 运行效果
- citeseer:  
<img src="pic/citeseer.png" width="628" height="288" alt="citeseer"/><br/>
- cora:  
<img src="pic/cora.png" width="628" height="288" alt="cora"/><br/>
- pubmed:  
<img src="pic/pubmed.png" width="628" height="288" alt="pubmed"/><br/>

## 参与贡献
wang567
