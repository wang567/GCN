import torch
import torch.nn as nn
from dgl.nn.pytorch import GraphConv


class GCN(nn.Module):
    def __init__(self,
                 g,
                 in_feats,
                 n_hidden,
                 n_classes,
                 n_layers,
                 activation,
                 dropout):
        """
        GCN模块
        :param g: 输入图
        :param in_feats: 输入特征维度
        :param n_hidden: 隐藏层维度
        :param n_classes: 输出特征维度（类别数量）
        :param n_layers: 隐藏层层数
        :param activation: 激活函数类型
        :param dropout: 是否使用dropout
        """
        super(GCN, self).__init__()
        self.g = g
        self.layers = nn.ModuleList()
        # input layer
        self.layers.append(GraphConv(in_feats, n_hidden, activation=activation))
        # hidden layers
        for i in range(n_layers - 1):
            self.layers.append(GraphConv(n_hidden, n_hidden, activation=activation))
        # output layer
        self.layers.append(GraphConv(n_hidden, n_classes))
        self.dropout = nn.Dropout(p=dropout)

    def forward(self, features):
        h = features
        for i, layer in enumerate(self.layers):
            if i != 0:  # 不对第一层做dropout
                h = self.dropout(h)
            h = layer(self.g, h)
        return h
